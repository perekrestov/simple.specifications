﻿using System;
using System.Linq.Expressions;

namespace Simple.Specifications.Tests.Specs
{
	internal class LessThenSpecification : SpecificationBase<int>
	{
		private readonly int _threshold;

		public LessThenSpecification(int threshold)
		{
			_threshold = threshold;
		}

		protected override Expression<Func<int, bool>> SpecExpression
		{
			get { return i => i < _threshold; }
		}
	}
}