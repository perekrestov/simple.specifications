﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace Simple.Specifications.Tests.Specs
{
	[TestFixture]
	public class CompositionTests
	{
		[Test]
		public void Union()
		{
			int[] data = new[] { 5, 6, 3, 2, 6, 7, 4 };
			SpecificationBase<int> spec = new LessThenSpecification(6) && new GreaterThenSpecification(3);

			IEnumerable<int> result = data.Where(spec);

			result.Should().HaveCount(2);
		}
	}
}