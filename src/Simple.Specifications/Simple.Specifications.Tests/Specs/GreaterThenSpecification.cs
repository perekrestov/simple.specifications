﻿using System;
using System.Linq.Expressions;

namespace Simple.Specifications.Tests.Specs
{
	internal class GreaterThenSpecification : SpecificationBase<int>
	{
		private readonly int _threshold;

		public GreaterThenSpecification(int threshold)
		{
			_threshold = threshold;
		}

		protected override Expression<Func<int, bool>> SpecExpression
		{
			get { return i => i > _threshold; }
		}
	}
}