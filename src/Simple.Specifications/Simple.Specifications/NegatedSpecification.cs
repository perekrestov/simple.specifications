﻿using System;
using System.Linq.Expressions;
using Simple.Specifications.Expressions;

namespace Simple.Specifications
{
	public class NegatedSpecification<T> : SpecificationBase<T>
	{
		private readonly ISpecification<T> _inner;

		public NegatedSpecification(ISpecification<T> inner)
		{
			_inner = inner;
		}

		protected override Expression<Func<T, bool>> SpecExpression
		{
			get { return _inner.SpecExpression.Not(); }
		}
	}
}