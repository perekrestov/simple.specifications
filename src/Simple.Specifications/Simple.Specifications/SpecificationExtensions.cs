﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Simple.Specifications.Expressions;

namespace Simple.Specifications
{
	public static class SpecificationExtensions
	{
		public static SpecificationBase<T> And<T>(
			this ISpecification<T> left,
			ISpecification<T> right)
		{
			return new AndSpecification<T>(left, right);
		}

		public static SpecificationBase<T> Or<T>(
			this ISpecification<T> left,
			ISpecification<T> right)
		{
			return new OrSpecification<T>(left, right);
		}

		public static SpecificationBase<T> Negate<T>(this ISpecification<T> inner)
		{
			return new NegatedSpecification<T>(inner);
		}

		public static SpecificationBase<T> AsSpec<T>(this Expression<Func<T, bool>> expression)
		{
			return new ExpressionSpecification<T>(expression);
		}

		public static SpecificationBase<T> AllSpec<T>(this IEnumerable<SpecificationBase<T>> specification)
		{
			var res = specification.Select(s => ((ISpecification<T>) s).SpecExpression).All();
			return new ExpressionSpecification<T>(res);
		}

		public static SpecificationBase<T> AnySpec<T>(this IEnumerable<SpecificationBase<T>> specification)
		{
			var res = specification.Select(s => ((ISpecification<T>)s).SpecExpression).Any();
			return new ExpressionSpecification<T>(res);
		}
	}
}