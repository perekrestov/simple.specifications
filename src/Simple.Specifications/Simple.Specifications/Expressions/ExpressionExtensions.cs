﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Simple.Specifications.Utils;

namespace Simple.Specifications.Expressions
{
	public static class ExpressionExtensions
	{
		public static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
		{
			// build parameter map (from parameters of second to parameters of first)
			Dictionary<ParameterExpression, ParameterExpression> map = first.Parameters.Select((f, i) => new {f, s = second.Parameters[i]}).ToDictionary(p => p.s, p => p.f);

			// replace parameters in the second lambda expression with parameters from the first
			Expression secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);

			// apply composition of lambda expression bodies to parameters from the first expression 
			return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
		}

		public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
		{
			return first.Compose(second, Expression.AndAlso);
		}

		public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
		{
			return first.Compose(second, Expression.OrElse);
		}

		public static Expression<Func<TEntity, bool>> Not<TEntity>(this Expression<Func<TEntity, bool>> expression)
		{
			UnaryExpression unaryExpression = Expression.Not(expression.Body);

			return Expression.Lambda<Func<TEntity, bool>>(unaryExpression, expression.Parameters);
		}

		public static Expression<Func<T, bool>> All<T>(this IEnumerable<Expression<Func<T, bool>>> specification)
		{
			return specification.Aggregate<Expression<Func<T, bool>>, Expression<Func<T, bool>>>(null, (current, expression) => current == null ? expression : current.And(expression));
		}

		public static Expression<Func<T, bool>> Any<T>(this IEnumerable<Expression<Func<T, bool>>> specification)
		{
			return specification.Aggregate<Expression<Func<T, bool>>, Expression<Func<T, bool>>>(null, (current, expression) => current == null ? expression : current.Or(expression));
		}
	}
}