﻿using System;
using System.Linq.Expressions;

namespace Simple.Specifications
{
	internal class ExpressionSpecification<T> : SpecificationBase<T>
	{
		private readonly Expression<Func<T, bool>> _inner;

		public ExpressionSpecification(Expression<Func<T, bool>> inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			_inner = inner;
		}

		protected override Expression<Func<T, bool>> SpecExpression
		{
			get { return _inner; }
		}
	}
}