﻿using System;
using System.Linq.Expressions;
using Simple.Specifications.Expressions;

namespace Simple.Specifications
{
	public class OrSpecification<T> : CompositeSpecificationBase<T>
	{
		public OrSpecification(
			ISpecification<T> left,
			ISpecification<T> right)
			: base(left, right)
		{
		}

		protected override Expression<Func<T, bool>> SpecExpression
		{
			get { return Left.SpecExpression.Or(Right.SpecExpression); }
		}
	}
}