using System;
using System.Linq.Expressions;

namespace Simple.Specifications
{
	public abstract class SpecificationBase<T> : ISpecification<T>
	{
		private Func<T, bool> _compiledExpression;

		private Func<T, bool> CompiledExpression => _compiledExpression ?? (_compiledExpression = SpecExpression.Compile());

		protected abstract Expression<Func<T, bool>> SpecExpression { get; }

		Expression<Func<T, bool>> ISpecification<T>.SpecExpression => SpecExpression;

		public bool IsSatisfiedBy(T obj)
		{
			return CompiledExpression(obj);
		}

		public static implicit operator Expression<Func<T, bool>>(SpecificationBase<T> rhs)
		{
			return rhs.SpecExpression;
		}

		public static implicit operator Func<T, bool>(SpecificationBase<T> rhs)
		{
			return rhs.CompiledExpression;
		}

		public static SpecificationBase<T> operator &(SpecificationBase<T> one, SpecificationBase<T> other)
		{
			return one.And(other);
		}

		public static SpecificationBase<T> operator |(SpecificationBase<T> one, SpecificationBase<T> other)
		{
			return one.Or(other);
		}

		public static SpecificationBase<T> operator !(SpecificationBase<T> one)
		{
			return one.Negate();
		}
		
		public static bool operator false(SpecificationBase<T> criteria)
		{
			return false;
		}
		
		public static bool operator true(SpecificationBase<T> criteria)
		{
			return false;
		}
	}
}