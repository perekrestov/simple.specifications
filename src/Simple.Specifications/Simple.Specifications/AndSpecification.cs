﻿using System;
using System.Linq.Expressions;
using Simple.Specifications.Expressions;

namespace Simple.Specifications
{
	public class AndSpecification<T> : CompositeSpecificationBase<T>
	{
		public AndSpecification(
			ISpecification<T> left,
			ISpecification<T> right)
			: base(left, right)
		{
		}

		protected override Expression<Func<T, bool>> SpecExpression
		{
			get { return Left.SpecExpression.And(Right.SpecExpression); }
		}
	}
}